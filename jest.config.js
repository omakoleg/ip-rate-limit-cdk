module.exports = {
  testEnvironment: "node",
  moduleDirectories: ["node_modules"],
  moduleFileExtensions: [
    "ts",
    "js",
    "node"
  ],
  roots: ['<rootDir>/test'],
  testMatch: ['**/*.test.ts'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  }
};
