import { isThresholdReached } from '../../src/lambda-blocker/lib';
import { MAX_THRESHOLD } from '../../src/lambda-shared/constants';

describe('lambda-blocker', () => {
  describe('.isThresholdReached', () => {
    it('false, single item', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 20,
            rate: 100,
          },
        ],
        () => 30,
      );
      expect(result).toBe(false);
    });

    it('false, multiple items within RATE_EXPIRATION_INTERVAL_SEC (300 sec)', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 800,
            rate: 10,
          },
          {
            time: 600,
            rate: 20,
          },
          {
            time: 500,
            rate: 30,
          },
        ],
        () => 1000,
      );
      expect(result).toBe(false);
    });

    it('false, multiple items outside RATE_EXPIRATION_INTERVAL_SEC (300 sec)', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 800,
            rate: 100,
          },
          {
            time: 400, // 800 - 400 interval, too big, immediate exit
            rate: 200_000_000,
          },
          {
            time: 500,
            rate: 300,
          },
        ],
        () => 1000,
      );
      expect(result).toBe(false);
    });

    it('false, multiple items combined above MAX_THRESHOLD (3000) but with a gap more than RATE_EXPIRATION_INTERVAL_SEC', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 900,
            rate: 1000,
          },
          {
            time: 700,
            rate: 1000,
          },
          {
            time: 200,
            rate: 1000,
          },
          {
            time: 100,
            rate: 1000,
          },
        ],
        () => 1000,
      );
      expect(result).toBe(false);
    });

    it('true, single above threshold violation', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 800,
            rate: MAX_THRESHOLD + 100,
          },
        ],
        () => 1000,
      );
      expect(result).toBe(true);
    });

    it('true, multiple items combined above MAX_THRESHOLD (3000)', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 900,
            rate: 1000,
          },
          {
            time: 700,
            rate: 1000,
          },
          {
            time: 600,
            rate: 1000,
          },
          {
            time: 500,
            rate: 1000,
          },
        ],
        () => 1000,
      );
      expect(result).toBe(true);
    });

    it('true, multiple items combined above MAX_THRESHOLD (3000) and contain more data by not expired TTL lag', () => {
      const result = isThresholdReached(
        '',
        [
          {
            time: 9_900,
            rate: 1000,
          },
          {
            time: 9_800,
            rate: 1000,
          },
          {
            time: 9_700,
            rate: 1000,
          },
          {
            time: 9_600,
            rate: 1000, // enough for blocking
          },
          {
            time: 200, // diff is 9_400 > RATE_EXPIRATION_INTERVAL_SEC
            rate: 10,
          },
          {
            time: 100,
            rate: 10,
          },
        ],
        () => 10_000,
      );
      expect(result).toBe(true);
    });
  });
});
