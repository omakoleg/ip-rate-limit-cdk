import * as cdk from '@aws-cdk/core';
import { Table, AttributeType, StreamViewType, BillingMode } from '@aws-cdk/aws-dynamodb';
import { join } from 'path';

import { NodejsFunction } from '@aws-cdk/aws-lambda-nodejs';
import { Rule, Schedule } from '@aws-cdk/aws-events';
import { LambdaFunction } from '@aws-cdk/aws-events-targets';
import { DynamoEventSource } from '@aws-cdk/aws-lambda-event-sources';
import { StartingPosition } from '@aws-cdk/aws-lambda';
import { RATE_TABLE_NAME, BLOCKED_TABLE_NAME } from '../src/lambda-shared/constants';
import { RemovalPolicy } from '@aws-cdk/core';

export class IpRateLimitCdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const rateTable = new Table(this, 'RateTable', {
      tableName: RATE_TABLE_NAME,
      partitionKey: { name: 'ip', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST,
      stream: StreamViewType.NEW_IMAGE,
      timeToLiveAttribute: 'expire',
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const blockedTable = new Table(this, 'BlockedTable', {
      tableName: BLOCKED_TABLE_NAME,
      partitionKey: { name: 'ip', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST,
      stream: StreamViewType.NEW_AND_OLD_IMAGES,
      timeToLiveAttribute: 'expire',
      removalPolicy: RemovalPolicy.DESTROY,
    });

    const processorLambda = new NodejsFunction(this, 'ProcessorLambda', {
      functionName: 'processor',
      entry: join(__dirname, '..', 'src/lambda-processor/index.ts'),
      handler: 'index',
    });
    rateTable.grantFullAccess(processorLambda);
    // See
    const rule = new Rule(this, 'ProcessorPeriodicRule', {
      schedule: Schedule.expression('cron(0/1 * ? * * *)'),
    });
    rule.addTarget(new LambdaFunction(processorLambda));

    const blockerLambda = new NodejsFunction(this, 'BlockerLambda', {
      functionName: 'blocker',
      entry: join(__dirname, '..', 'src/lambda-blocker/index.ts'),
      handler: 'index',
    });
    blockedTable.grantWriteData(blockerLambda);
    rateTable.grantWriteData(blockerLambda);
    blockerLambda.addEventSource(
      new DynamoEventSource(rateTable, {
        startingPosition: StartingPosition.TRIM_HORIZON,
        batchSize: 10,
        retryAttempts: 10,
      }),
    );

    const unblockerLambda = new NodejsFunction(this, 'UnblockerLambda', {
      functionName: 'unblocker',
      entry: join(__dirname, '..', 'src/lambda-unblocker/index.ts'),
      handler: 'index',
    });
    unblockerLambda.addEventSource(
      new DynamoEventSource(blockedTable, {
        startingPosition: StartingPosition.TRIM_HORIZON,
        batchSize: 1, // single item processing
        retryAttempts: 10,
      }),
    );
  }
}
