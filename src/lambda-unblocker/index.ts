import { DynamoDBStreamEvent } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { buildApiService } from '../lambda-shared/api-service';
import { BlockedDatabaseRecord } from '../lambda-shared/constants';
import { pretty } from '../lambda-shared/helpers';

const apiService = buildApiService();

export const index = async (event: DynamoDBStreamEvent) => {
  console.log('unblocker', pretty(event));
  const single = event.Records[0];

  if (single.eventName !== 'REMOVE') {
    return console.log(`Skip: ${single.eventName}`, single.dynamodb);
  }
  const record = DynamoDB.Converter.unmarshall(single.dynamodb!.OldImage!) as unknown as BlockedDatabaseRecord;
  console.log(`Unblocking: ${record.ip}`);
  await apiService.unblockIP(record.ip);
};
