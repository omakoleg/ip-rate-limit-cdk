import { buildApiService, LogEntry } from '../lambda-shared/api-service';
import { DynamoDB } from 'aws-sdk';
import {
  INTERVAL_IP_ERROR_THRESHOLD,
  RATE_EXPIRATION_INTERVAL_SEC,
  RATE_TABLE_NAME,
  RateDatabaseRecord,
} from '../lambda-shared/constants';
import { getTimeSeconds, pretty } from '../lambda-shared/helpers';

const apiService = buildApiService();
const docClient = new DynamoDB.DocumentClient();

type IpsBuffer = Record<string, number>;
interface IpRecord {
  ip: string;
  rate: number;
}

export const index = async () => {
  console.log('processor starting');

  const logs = await apiService.getLogs();
  console.log(logs);

  // group by IP
  const aggregated = logs.reduce((acc: IpsBuffer, c: LogEntry) => {
    if (!acc[c.ip]) {
      acc[c.ip] = 0;
    }
    acc[c.ip] += 1;
    return acc;
  }, {});
  // convert to an array
  const aggregatedArray: IpRecord[] = Object.keys(aggregated).map((ip) => ({
    ip,
    rate: aggregated[ip],
  }));
  console.log({ aggregatedArray });

  const aboveThreshold = aggregatedArray.filter((x) => x.rate >= INTERVAL_IP_ERROR_THRESHOLD);
  console.log({ aboveThreshold });

  const timeSeconds = getTimeSeconds();
  // Insert cuttent time error rate into the `rates` and update `expire`
  const executors = aboveThreshold.map(async ({ ip, rate }) => {
    const result = await docClient
      .get({
        TableName: RATE_TABLE_NAME,
        Key: {
          ip,
        },
      })
      .promise();
    const newItem: RateDatabaseRecord = {
      ip,
      expire: timeSeconds + RATE_EXPIRATION_INTERVAL_SEC,
      rates: {
        [timeSeconds]: rate,
      },
    };
    if (result.Item) {
      const existingData = result.Item as RateDatabaseRecord;
      console.log('Item exists', pretty(existingData));
      newItem.rates = {
        ...existingData.rates,
        ...newItem.rates,
      };
    }
    console.log('newItem', pretty(newItem));
    await docClient
      .put({
        TableName: RATE_TABLE_NAME,
        Item: newItem,
      })
      .promise();
  });
  await Promise.all(executors);
};
