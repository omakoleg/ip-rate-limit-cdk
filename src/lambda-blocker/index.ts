import { DynamoDBStreamEvent } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { buildApiService } from '../lambda-shared/api-service';
import { RateDatabaseRecord } from '../lambda-shared/constants';
import { getTimeSeconds, pretty } from '../lambda-shared/helpers';
import { processSingleRecord } from './lib';

const apiService = buildApiService();
const docClient = new DynamoDB.DocumentClient();

export const index = async (event: DynamoDBStreamEvent) => {
  for (const item of event.Records.filter((x) => x.eventName === 'REMOVE')) {
    console.log('expired', pretty(item.dynamodb?.OldImage));
  }
  for (const item of event.Records.filter((x) => x.eventName !== 'REMOVE')) {
    await processSingleRecord(
      apiService,
      docClient,
      DynamoDB.Converter.unmarshall(item.dynamodb!.NewImage!) as unknown as RateDatabaseRecord,
      getTimeSeconds,
    );
  }
};
