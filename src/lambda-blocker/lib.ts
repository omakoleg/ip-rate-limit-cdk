import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { ApiService } from '../lambda-shared/api-service';
import {
  BLOCKED_TABLE_NAME,
  BLOCKING_INTERVAL_SEC,
  MAX_THRESHOLD,
  RateDatabaseRecord,
  RATE_EXPIRATION_INTERVAL_SEC,
  RATE_TABLE_NAME,
} from '../lambda-shared/constants';
import { pretty } from '../lambda-shared/helpers';

export interface TimeErrorRate {
  time: number;
  rate: number;
}

export const isThresholdReached = (ip: string, timeErrorRates: TimeErrorRate[], getTimeSec: () => number): boolean => {
  let totalRate = 0;
  let previousTime = getTimeSec();
  console.log(timeErrorRates);
  for (const rateItem of timeErrorRates) {
    if (previousTime - RATE_EXPIRATION_INTERVAL_SEC > rateItem.time) {
      // there was a "Gap" longer than cooldown period
      // item is not valid and will be removed by TTL
      // anyway check that total witin RATE_EXPIRATION_INTERVAL_SEC
      break;
    }
    totalRate += rateItem.rate;
    previousTime = rateItem.time;
  }
  console.log(`isThresholdReached ip: ${ip} ${totalRate} > ${MAX_THRESHOLD}`);
  return totalRate > MAX_THRESHOLD;
};

export const processSingleRecord = async (
  apiService: ApiService,
  docClient: DocumentClient,
  record: RateDatabaseRecord,
  getTimeSec: () => number,
): Promise<void> => {
  // transform to sorted array of error rates per timestamp
  const timeErrorRates: TimeErrorRate[] = Object.keys(record.rates)
    .map((x) => parseInt(x))
    .sort((n1: number, n2: number) => n1 - n2)
    .reverse()
    .map((time) => ({
      time: time,
      rate: record.rates[time.toString()],
    }));
  console.log({ record, timeErrorRates });

  if (isThresholdReached(record.ip, timeErrorRates, getTimeSec)) {
    // Api can be called multiple times to block the same IP
    // till this IP would not appear in logs
    await apiService.blockIP(record.ip);
    const item = {
      ip: record.ip,
      expire: getTimeSec() + BLOCKING_INTERVAL_SEC,
    };
    await docClient
      .put({
        TableName: BLOCKED_TABLE_NAME,
        Item: item,
      })
      .promise();
    console.log(`Added to blocked:`, pretty(item));
    await docClient
      .delete({
        TableName: RATE_TABLE_NAME,
        Key: {
          ip: record.ip,
        },
      })
      .promise();
    console.log(`Removed from rates: ${record.ip}`);
  } else {
    console.log(`Blocking threshodl is not reached by: `, pretty(record), pretty(timeErrorRates));
  }
};
