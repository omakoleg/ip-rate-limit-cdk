export const getTimeSeconds = () => Math.floor(Date.now() / 1000);
export const pretty = (obj: any) => JSON.stringify(obj, undefined, 4);
