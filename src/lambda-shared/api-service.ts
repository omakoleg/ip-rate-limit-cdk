type ErrorCode = 200 | 401 | 403;
export interface LogEntry {
  ip: string;
  code: ErrorCode;
  message?: string;
}

export interface ApiService {
  getLogs: () => Promise<LogEntry[]>;
  blockIP: (ip: string) => Promise<void>;
  unblockIP: (ip: string) => Promise<void>;
}

const randomInt = (min = 1, max = 250) => Math.floor(Math.random() * (max - min + 1)) + min;
const randomErrorStatus = (): ErrorCode => {
  const arr: ErrorCode[] = [401, 403];
  return arr[Math.floor(Math.random() * arr.length)];
};

export const buildApiService = (): ApiService => {
  // Return each time random 100 IP addresses errors
  const getLogs = async (): Promise<LogEntry[]> =>
    [...Array(100).keys()].map((i) => ({
      ip: `1.2.3.${randomInt(1, 10)}`,
      code: randomErrorStatus(),
    }));

  const blockIP = async (ip: string) => {
    console.log(`IP ${ip} is blocked`);
  };
  const unblockIP = async (ip: string) => {
    console.log(`IP ${ip} is unblocked`);
  };

  return {
    getLogs,
    blockIP,
    unblockIP,
  };
};
