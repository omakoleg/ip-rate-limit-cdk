export const RATE_TABLE_NAME = 'rate';
export const BLOCKED_TABLE_NAME = 'blocked';

// how fast `rate` item is expiring
export const RATE_EXPIRATION_INTERVAL_SEC = 300; // 5 min
// Min. Error rate per minute to register in `rate`
export const INTERVAL_IP_ERROR_THRESHOLD = 10;
// Max. combined error threshold
export const MAX_THRESHOLD = 3_000;
// How long IP should be blocked
// And "eventually" after TTL it will be deleted.
export const BLOCKING_INTERVAL_SEC = 900; // 15 minutes

export interface RateDatabaseRecord {
  ip: string;
  expire: number;
  rates: Record<string, number>;
}

export interface BlockedDatabaseRecord {
  ip: string;
  expire: number;
}
